//
//  recipe.cpp
//  Variables Lab 1: Recipe Program
//
//  Created by Logan on 9/7/20.
//  Copyright © 2020 Logan. All rights reserved.
//

#include <iostream>
using namespace std;

int main()
{
    // 1. Declare ingredients variables
    
    float cupsOfButter = 1;
    float cupsOfSugar = 1;
    float eggs = 2;
    float tspsVanilla = 2;
    float tspsBakingSoda = 1;
    float tspsSalt = 0.5;
    float cupsFlour = 3;
    float cupsChocolateChips = 2;
    
    // 2. Display ingredients to the screen
    
    cout << cupsOfButter << " cups(s) of butter" << endl;
    cout << cupsOfSugar << " cup(s) of sugar" << endl;
    cout << eggs << " eggs" << endl;
    cout << tspsVanilla << " teaspoon(s) of vanilla" << endl;
    cout << tspsBakingSoda << " teaspoon(s) of baking soda" << endl;
    cout << tspsSalt << " teaspoon(s) of salt" << endl;
    cout << cupsFlour << " cup(s) of flour" << endl;
    cout << cupsChocolateChips << " cup(s) of chocolate chips" << endl;
    
    // 3. Create a variable for "batchSize"
    
    float batchSize;
    
    // 4. Ask the user how many batches to make
    
    cout << "How many batches of cookies? ";
    cin >> batchSize;
    
    // 5. Recalculate ingredient amounts
    
    cupsOfButter = cupsOfButter * batchSize;
    cupsOfSugar = cupsOfSugar * batchSize;
    eggs = eggs * batchSize;
    tspsVanilla = tspsVanilla * batchSize;
    tspsBakingSoda = tspsBakingSoda * batchSize;
    tspsSalt = tspsSalt * batchSize;
    cupsFlour = cupsFlour * batchSize;
    cupsChocolateChips = cupsChocolateChips * batchSize;
    
    // 6. Display updated ingredients list
    
    cout << cupsOfButter << " cups(s) of butter" << endl;
    cout << cupsOfSugar << " cup(s) of sugar" << endl;
    cout << eggs << " eggs" << endl;
    cout << tspsVanilla << " teaspoon(s) of vanilla" << endl;
    cout << tspsBakingSoda << " teaspoon(s) of baking soda" << endl;
    cout << tspsSalt << " teaspoon(s) of salt" << endl;
    cout << cupsFlour << " cup(s) of flour" << endl;
    cout << cupsChocolateChips << " cup(s) of chocolate chips" << endl;
    
    return 0;
    
    // extra comments 
}
